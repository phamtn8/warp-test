/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.warp;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author LeVyAnh
 */
@ManagedBean
@ViewScoped
public class MyBean implements Serializable {
    
    private String name;
    
    public MyBean() {
    }
    
    @PostConstruct
    public void init(){
        System.out.println("Inside MyBean init");
        name = "Harry";
    }
    
    public String foo(){
        System.out.println("Inside MyBean executing foo ...");
        return "foo";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
}
