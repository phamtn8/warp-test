/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.warp.dummy;


import com.test.warp.MyBean;
import javax.faces.bean.ManagedProperty;
import org.jboss.arquillian.warp.Inspection;
import org.jboss.arquillian.warp.jsf.AfterPhase;
import org.jboss.arquillian.warp.jsf.Phase;
import org.jboss.arquillian.warp.servlet.AfterServlet;
import org.junit.Assert;

/**
 *
 * @author LeVyAnh
 */
public class MyInspection2 extends Inspection{
    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{myBean}")
    private MyBean myBean;
    
    
    @AfterServlet
    public void testBean(){
        Assert.assertNotNull(myBean);
    }
    
    @AfterPhase(Phase.RENDER_RESPONSE)
    public void test(){
        Assert.assertTrue(true);
    }
}
