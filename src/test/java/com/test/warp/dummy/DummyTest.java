/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.warp.dummy;

import com.test.warp.MyBean;
import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.warp.Activity;
import org.jboss.arquillian.warp.Warp;
import org.jboss.arquillian.warp.WarpTest;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author LeVyAnh
 */
@RunWith(Arquillian.class)
@WarpTest
@RunAsClient
public class DummyTest {

    @Drone
    static WebDriver browser;
    @ArquillianResource
    URL contextPath;
    private static final String WEBAPP_SRC = "src/main/webapp";

    @Deployment(testable = true)
    public static WebArchive deploy() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "dummyTest.war");
        war.addClasses(MyBean.class, MyInspection.class, MyInspection2.class);
        war.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                .importDirectory(WEBAPP_SRC).as(GenericArchive.class),
                "/", Filters.includeAll());
        System.out.println(war.toString(true));
        return war;
    }
    
    @Test
    public void dummyTest(){
        System.out.println("Context Path: " + contextPath);
        Warp.initiate(new Activity(){
            
            @Override
            public void perform(){                
                browser.navigate().to(contextPath + "/faces/index.xhtml");
                
            
            }
        }).inspect(new MyInspection());
        
        Warp.initiate(new Activity(){

            @Override
            public void perform() {
                browser.findElement(By.id("testLink")).click();
            }
            
        }).inspect(new MyInspection2());
    }
}
